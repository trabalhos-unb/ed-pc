/*
* Programção Concorrente
* Estudo Dirigido 9 - Pombo Correito(usando semáforo)
* Aluno: Danilo Santos de Sales 
* Matricula: 14/0135910
*/

#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include <stdbool.h>
#include <semaphore.h>

#define PIGEON 1
#define WRITERS 5
#define MAX_LETTERS 20
#define SEM_MUTEX 1

sem_t sem_bag, sem_mutex, sem_pigeon;
pthread_t threads[WRITERS + PIGEON];

int bag = 0;

void *writer(void *);
void *pigeon(void *);

int main(void)
{
    sem_init(&sem_bag, 0, MAX_LETTERS);
    sem_init(&sem_mutex, 0, SEM_MUTEX);
    sem_init(&sem_pigeon, 0, 0);

    pthread_create(&threads[0], NULL, pigeon, 0);

    for (int i = PIGEON; i < PIGEON + WRITERS; i++)
    {
        pthread_create(&threads[i], NULL, writer, NULL);
    }
    for (int i = 0; i < PIGEON + WRITERS; i++)
    {
        pthread_join(threads[i], NULL);
    }
    sem_destroy(&sem_bag);
    sem_destroy(&sem_mutex);
    sem_destroy(&sem_pigeon);
    return EXIT_SUCCESS;
}

void *writer(void *arg)
{
    while (true)
    {
        sem_wait(&sem_bag);
        sem_wait(&sem_mutex);
        printf("Escritor colocou uma carta\t");
        sleep(1);
        bag = bag + 1;
        printf("Há atualmente %d cartas\n", bag);
        sem_post(&sem_mutex);
        //Acorda o pombo
        if (bag == MAX_LETTERS)
            sem_post(&sem_pigeon);
    }
    pthread_exit(EXIT_SUCCESS);
}
void *pigeon(void *arg)
{
    while (true)
    {
        sem_wait(&sem_pigeon);
        //Leva as carta de A para B
        sem_wait(&sem_mutex);
        printf("\nLevando carta de A para B\n");
        sleep(5);
        printf("Cartas entregues a B\n\n");
        bag = 0;
        sem_post(&sem_mutex);
        //Acorda os escritores
        sem_post(&sem_bag);
    }
    pthread_exit(EXIT_SUCCESS);
}
