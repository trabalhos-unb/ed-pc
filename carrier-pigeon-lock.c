/*
* Programção Concorrente
* Estudo Dirigido 7 - Pombo Correito(usando lock)
* Aluno: Danilo Santos de Sales 
* Matricula: 14/0135910
*/

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <stdbool.h>
#include <pthread.h>

#define PIGEON 1
#define WRITERS 5
#define MAX_LETTERS 20

pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t mutex_writer = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t mutex_bag = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t cond_writer = PTHREAD_COND_INITIALIZER;
pthread_cond_t cond_pigeon = PTHREAD_COND_INITIALIZER;
pthread_t threads[WRITERS];

int bag = 0;

void *writer(void *);
void *pigeon(void *);

int main(void)
{
    pthread_create(&threads[0], NULL, pigeon, NULL);

    for (int i = PIGEON; i < PIGEON + WRITERS; i++)
    {
        pthread_create(&threads[i], NULL, writer, NULL);
    }
    for (int i = 0; i < PIGEON + WRITERS; i++)
    {
        pthread_join(threads[i], NULL);
    }
    return EXIT_SUCCESS;
}

void *writer(void *arg)
{
    while (true)
    {
        pthread_mutex_lock(&mutex_bag);
        if (bag == MAX_LETTERS)
        {
            pthread_cond_signal(&cond_pigeon);
            pthread_cond_wait(&cond_writer, &mutex_writer);
        }
        printf("Escritor colocou uma carta\t");
        sleep(1);
        bag = bag + 1;
        printf("Há atualmente %d cartas\n", bag);
        pthread_mutex_unlock(&mutex_bag);
    }
    pthread_exit(EXIT_SUCCESS);
}
void *pigeon(void *arg)
{
    while (true)
    {
        if (bag != MAX_LETTERS)
        {
            pthread_cond_wait(&cond_pigeon, &mutex_writer);
        }
        pthread_mutex_lock(&mutex);
        printf("\nLevando carta de A para B\n");
        sleep(5);
        printf("Cartas entregues a B\n\n");
        bag = 0;
        pthread_mutex_unlock(&mutex);

        pthread_cond_broadcast(&cond_writer);
    }
    pthread_exit(EXIT_SUCCESS);
}
