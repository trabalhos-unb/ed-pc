/*
 * Programção Concorrente
 * Estudo Dirigido 11 - Jantar dos Filósofos
 * Aluno: Danilo Santos de Sales
 * Matricula: 14/0135910
 */

#include <pthread.h>
#include <semaphore.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define N 5
#define POSITION(i) (abs(i) + N) % N
#define LEFT(i) POSITION(i - 1)
#define RIGHT(i) POSITION(i + 1)

typedef enum state { THINKING = 0, STARVING = 1, EATING = 2 } state;

pthread_t threads[N];
state philosopher_state[N] = {THINKING, THINKING, THINKING, THINKING, THINKING};
int ids[N] = {0, 1, 2, 3, 4};
sem_t philosopher_turn[N];
sem_t mutex;

void *philosopher(void *);
void get_fork(int);
void can_i_eat(int);
void put_fork(int);

void init_sem();
void destroy_sem();

int main(void) {
    init_sem();
    for (int i = 0; i < N; i++) {
        pthread_create(&threads[i], NULL, philosopher, &ids[i]);
    }
    for (int i = 0; i < N; i++) {
        pthread_join(threads[i], NULL);
    }
    destroy_sem();
    return EXIT_SUCCESS;
}

void *philosopher(void *arg) {
    int id = *(int *)arg;
    printf("Inicializado\t Filósofo %d", id);
    while (true) {
        // sleep(5);
        printf("Filósofo %d está pensando", id);
        get_fork(id);
        printf("Filósofo %d está comendo", id);
        // sleep(2);
        put_fork(id);
    }
    pthread_exit(EXIT_SUCCESS);
}

void get_fork(int id) {
    sem_wait(&mutex);
    philosopher_state[id] = STARVING;
    can_i_eat(id);
    sem_post(&mutex);
    sem_wait(&philosopher_turn[id]);
    printf("Filósofo %d não conseguiu o garfo", id);
}

void can_i_eat(int id) {
    if (philosopher_state[id] == STARVING && philosopher_state[LEFT(id)] &&
        philosopher_state[RIGHT(id)]) {
        philosopher_state[id] = EATING;
        sem_post(&philosopher_turn[id]);
    }
}

void put_fork(int id) {
    sem_wait(&mutex);
    philosopher_state[id] = THINKING;
    can_i_eat(LEFT(id));
    can_i_eat(RIGHT(id));
    sem_post(&mutex);
}

void init_sem() {
    sem_init(&mutex, 0, 0);
    for (int i = 0; i < N; i++) {
        sem_init(&philosopher_turn[i], 0, 1);
    }
}

void destroy_sem() {
    sem_destroy(&mutex);
    for (int i = 0; i < N; i++) {
        sem_destroy(&philosopher_turn[i]);
    }
}
