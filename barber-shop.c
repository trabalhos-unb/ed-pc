/*
* Programção Concorrente
* Estudo Dirigido 7 - Barbeiro Dorminhoco
* Aluno: Danilo Santos de Sales 
* Matricula: 14/0135910
*/

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <pthread.h>
#include <semaphore.h>

#define NUMCLIENT 4
#define NUMBARBER 1
#define NUMBARBER_CHAIR NUMBARBER
#define NUMWAIT_CHAIR 3

sem_t sem_wait_chair;
pthread_mutex_t mutex_barber = PTHREAD_MUTEX_INITIALIZER;
pthread_mutex_t mutex_client = PTHREAD_MUTEX_INITIALIZER;
pthread_t threads[NUMCLIENT + NUMBARBER];

pthread_cond_t cond_client = PTHREAD_COND_INITIALIZER;
pthread_cond_t cond_barber = PTHREAD_COND_INITIALIZER;

void *client(void *);
void *barber(void *);

int main(void)
{
    //Init
    sem_init(&sem_wait_chair, 0, NUMWAIT_CHAIR);
    //Cria barbeiro
    pthread_create(&threads[0], NULL, barber, NULL);
    //Cria clientes
    for (int i = NUMBARBER; i < NUMCLIENT + NUMBARBER; i++)
    {
        pthread_create(&threads[i], NULL, client, NULL);
    }
    for (int i = 0; i < NUMCLIENT + NUMBARBER; i++)
    {
        pthread_join(threads[i], NULL);
    }
    sem_destroy(&sem_wait_chair);
    return EXIT_SUCCESS;
}

void *barber(void *arg)
{
    printf("Barber is cutting...");
    sleep(3);
    pthread_cond_wait(&cond_client, &mutex_client);

    pthread_cond_wait(&cond_barber, &mutex_barber);
    pthread_exit(EXIT_SUCCESS);
}

void *client(void *arg)
{
    //Verifica se há cadeiras vazias
    sem_trywait(&sem_wait_chair);

    pthread_mutex_lock(&mutex_client);
    pthread_cond_signal(&cond_barber);
    printf("Client %d, calls for barber", 1);

    pthread_mutex_unlock(&mutex_client);
    //Chama alguem de fora da barbearia
    sem_post(&sem_wait_chair);
    pthread_exit(EXIT_SUCCESS);
}
